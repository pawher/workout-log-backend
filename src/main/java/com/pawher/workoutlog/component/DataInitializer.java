package com.pawher.workoutlog.component;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Role;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.repository.AppUserRepository;
import com.pawher.workoutlog.repository.RoleRepository;
import com.pawher.workoutlog.repository.WorkTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {
    private RoleRepository roleRepository;
    private AppUserRepository appUserRepository;
    private WorkTypeRepository workTypeRepository;

    public DataInitializer(RoleRepository roleRepository, AppUserRepository appUserRepository, WorkTypeRepository workTypeRepository) {
        this.roleRepository = roleRepository;
        this.appUserRepository = appUserRepository;
        this.workTypeRepository = workTypeRepository;
    }

    @Autowired


    private void loadData() {
        Role adminRole = new Role("ADMIN");
//        adminRole = roleRepository.save(adminRole);

        roleRepository.save(new Role("USER"));
        roleRepository.save(new Role("GUEST"));

        workTypeRepository.save(new WorkoutType("Gym"));
        workTypeRepository.save(new WorkoutType("Running"));
        workTypeRepository.save(new WorkoutType("Fitness"));
        workTypeRepository.save(new WorkoutType("Walking"));
        workTypeRepository.save(new WorkoutType("Tennis"));
        workTypeRepository.save(new WorkoutType("Other"));

        appUserRepository.save(new AppUser("admin", "admin", adminRole));
    }
}
