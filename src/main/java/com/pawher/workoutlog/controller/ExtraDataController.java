package com.pawher.workoutlog.controller;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.WeightTarget;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.*;
import com.pawher.workoutlog.model.response.ResponseMessage;
import com.pawher.workoutlog.model.response.StatusResponse;
import com.pawher.workoutlog.service.AppUserService;
import com.pawher.workoutlog.service.ProgressLogService;
import com.pawher.workoutlog.service.WeightTargetService;
import com.pawher.workoutlog.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping(path = "/extra")
public class ExtraDataController {

    private WorkoutService workoutService;
    private AppUserService appUserService;
    private WeightTargetService weightTargetService;
    private ProgressLogService progressLogService;

    @Autowired
    public ExtraDataController(WorkoutService workoutService, AppUserService appUserService, WeightTargetService weightTargetService, ProgressLogService progressLogService) {
        this.workoutService = workoutService;
        this.appUserService = appUserService;
        this.weightTargetService = weightTargetService;
        this.progressLogService = progressLogService;
    }


    @RequestMapping(path = "/numberofworkouts", method = RequestMethod.GET)
    public ResponseEntity<java.lang.Integer> listAll() {
        PageResponse<Workout> list = workoutService.getAllWorkouts();
        int number = list.getTotalElements();
        return RespFactory.resultint(number);
    }

    @RequestMapping(path = "/date", method = RequestMethod.GET)
    public ResponseEntity<java.time.LocalDate> getDate() {
        LocalDate date = LocalDate.now();
        return RespFactory.result(date);
    }

    @RequestMapping(path = "target", method = RequestMethod.GET)
    public ResponseEntity<java.lang.Integer> getTarget() {
        Integer list = weightTargetService.getAllTargetsAsList();
        return RespFactory.result(list);


    }

    @RequestMapping(path = "time", method = RequestMethod.GET)
    public ResponseEntity<java.lang.Integer> getTime() {
        Integer list = workoutService.getTime();
        return RespFactory.result(list);


    }

//    @RequestMapping(path = "current", method = RequestMethod.GET)
//    public ResponseEntity<java.lang.Integer> getCurrentWeight() {
//        Integer current = progressLogService.getCurrent();
//        Integer target = weightTargetService.getAllTargetsAsList();
//
//        return RespFactory.result(target - current);
//    }

    @RequestMapping(path = "current", method = RequestMethod.GET)
    public ResponseEntity<java.lang.Integer> getCurrentWeight(@RequestParam(name = "userId") Long id) {
        Integer current = null;
        try {
            current = progressLogService.getCurrentByUserID(id);
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }

        Integer target = null;
        try {
            target = weightTargetService.getAllTargetsAsListByUserID(id);
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }

        return RespFactory.result(target - current);
    }



    @RequestMapping(path = "typecounter", method = RequestMethod.GET)
    public ResponseEntity getGymCounter(@RequestParam(name = "type") String workoutType, @RequestParam(name = "userId") Long user) {
        Optional<WorkoutType> workoutOptional = workoutService.getWorkoutByName(workoutType);
        if (workoutOptional.isPresent()) {
            int count = workoutService.getWorkoutsCountByType(workoutOptional.get());
            return RespFactory.result(count);
        }
        return RespFactory.badRequest();
    }

    @RequestMapping(path = "daystoend", method = RequestMethod.GET)
    public ResponseEntity<java.lang.Integer> getDaysToEnd() {
        LocalDate today = LocalDate.now();
        LocalDate targetDay = weightTargetService.getTargetDate();

        Period period = Period.between(today,targetDay);
        Integer days = period.getDays();


        return RespFactory.resultint(days);
    }
}









