package com.pawher.workoutlog.controller;


import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.ProgressLog;
import com.pawher.workoutlog.model.WeightTarget;
import com.pawher.workoutlog.model.dto.*;
import com.pawher.workoutlog.service.AppUserService;
import com.pawher.workoutlog.service.ProgressLogService;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Controller
@CrossOrigin
@RequestMapping(path = "/progress/")
public class ProgressLogController {

    private ProgressLogService progressLogService;
    private AppUserService userService;

    @Autowired
    public ProgressLogController(ProgressLogService progressLogService, AppUserService userService) {
        this.progressLogService = progressLogService;
        this.userService = userService;
    }


    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Response> addNewProgress(@RequestBody ProgressDto progress) {
        try {
            progressLogService.addNewProgress(progress);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<ProgressLog>> list(@RequestParam Long id) {
        try {
            List<ProgressLog> progressById = progressLogService.getAllProgressesByUserId(id);
            return RespFactory.result(progressById);
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }
        return RespFactory.badRequestProsses();
    }

    @RequestMapping(path = "/listall", method = RequestMethod.GET)
    public ResponseEntity<Response> listall() {
        PageResponse<ProgressLog> list = progressLogService.getAllProgresses();
        return RespFactory.result(list);
    }


}
