package com.pawher.workoutlog.controller;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.exceptions.WeightTargetDoesNotExistsException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.WeightTarget;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.RespFactory;
import com.pawher.workoutlog.model.dto.Response;
import com.pawher.workoutlog.model.dto.WeightTargetDto;
import com.pawher.workoutlog.service.AppUserService;
import com.pawher.workoutlog.service.WeightTargetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
@RequestMapping(path = "/target/")
public class WeightTargetController {

    private WeightTargetService weightTargetService;
    private AppUserService appUserService;

    @Autowired
    public WeightTargetController(WeightTargetService weightTargetService, AppUserService appUserService) {
        this.weightTargetService = weightTargetService;
        this.appUserService = appUserService;
    }


    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Response> addNewWeightTarget(@RequestBody WeightTargetDto weightTargetDto) {
        try {
            weightTargetService.addWeightTarget(weightTargetDto);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public ResponseEntity<WeightTarget> weightTarget(@RequestParam Long id) {
        Optional<WeightTarget> target = weightTargetService.getWeightTargetByUserID(id);
        if (target.isPresent()) {
            return RespFactory.result(target.get());
        }
        return RespFactory.badRequestWeight();
    }

    @RequestMapping(path = "/getall", method = RequestMethod.GET)
    public ResponseEntity<Response> listAll() {
        PageResponse<WeightTarget> list = weightTargetService.getAllTargets();
        return RespFactory.result(list);
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Response> weightTargetUpdate(@RequestBody WeightTarget weightTarget, @RequestParam Long id) {
        try {
            weightTargetService.updateWeightTarget(weightTarget, id);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        } catch (WeightTargetDoesNotExistsException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity list(@RequestParam(name = "userid") Long userid) {
        Optional<AppUser> user = appUserService.getUserWithId(userid);
        if (!user.isPresent()) {
            return RespFactory.badRequest();
        }
        return RespFactory.result(user.get().getWeightTarget());
    }


}

