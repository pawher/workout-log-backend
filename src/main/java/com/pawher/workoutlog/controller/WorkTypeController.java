package com.pawher.workoutlog.controller;


import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.RespFactory;
import com.pawher.workoutlog.model.dto.Response;
import com.pawher.workoutlog.model.dto.WorkoutTypeDto;
import com.pawher.workoutlog.service.WorkoutTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@RequestMapping(path = "/type/")
public class WorkTypeController {

    private WorkoutTypeService workoutTypeService;

    @Autowired
    public WorkTypeController(WorkoutTypeService workoutTypeService) {
        this.workoutTypeService = workoutTypeService;
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<Response> list() {
        PageResponse<WorkoutType> list = workoutTypeService.getAllType();
        return RespFactory.result(list);

    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Response> addNewType (@RequestBody WorkoutTypeDto workoutTypeDto) {
        try {
            workoutTypeService.addNewType(workoutTypeDto);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }
}
