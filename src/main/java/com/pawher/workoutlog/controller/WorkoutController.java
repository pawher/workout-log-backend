package com.pawher.workoutlog.controller;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.RespFactory;
import com.pawher.workoutlog.model.dto.Response;
import com.pawher.workoutlog.model.dto.WorkoutDto;
import com.pawher.workoutlog.service.AppUserService;
import com.pawher.workoutlog.service.WorkoutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
@RequestMapping(path = "/workout/")
public class WorkoutController {

    private WorkoutService workoutService;
    private AppUserService appUserService;

    @Autowired
    public WorkoutController(WorkoutService workoutService, AppUserService appUserService) {
        this.workoutService = workoutService;
        this.appUserService = appUserService;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Response> addNewWorkout(@RequestBody WorkoutDto workout) {
        try {
            workoutService.addNewWorkout(workout);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity list(@RequestParam(name = "userid") Long userid) {

        Optional<AppUser> user = appUserService.getUserWithId(userid);
        if(!user.isPresent()){
            return RespFactory.badRequest();
        }
        return RespFactory.result(user.get().getWorkoutList().stream().sorted((o1, o2) -> {
            if(o1.getWorkoutDate().isAfter(o2.getWorkoutDate())) return -1;
            else if (o2.getWorkoutDate().isAfter(o1.getWorkoutDate())) return 1;
            return 0;
        }).collect(Collectors.toList()));
    }

    @RequestMapping(path = "/listall", method = RequestMethod.GET)
    public ResponseEntity<Response> listAll() {
        PageResponse<Workout> list = workoutService.getAllWorkouts();
        return RespFactory.result(list);
    }

    @RequestMapping(path = "/remove", method = RequestMethod.DELETE)
    public ResponseEntity<Response> remove(@RequestParam Long id) {
        Optional<Workout> workoutOptional = workoutService.getWorkoutByID(id);

        if (!workoutOptional.isPresent()) {
            return RespFactory.badRequest();
        } else {
            Workout workout = workoutOptional.get();
            workoutService.remove(workout);
            return RespFactory.ok("removed!");
        }
    }

    @RequestMapping(path = "/page", method = RequestMethod.GET)
    public ResponseEntity<Response> page(@RequestParam(name = "page") int page) {
        PageResponse<Workout> list = workoutService.getWorkout(page);
        return RespFactory.result(list);
    }





}
