package com.pawher.workoutlog.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity (name = "appuser")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppUser implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String login;
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pass;

    @OneToMany (fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
@JsonProperty (access = JsonProperty.Access.WRITE_ONLY)
    private Set<Role> roleSet;

    @OneToMany (fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Workout> workoutList;

    @OneToOne (fetch = FetchType.EAGER)
    private WeightTarget weightTarget;

    @OneToMany (fetch = FetchType.EAGER)
    private Set<ProgressLog> progressLogList;

    public AppUser(String login, String pass, Role role) {
        this.login = login;
        this.pass = pass;
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        this.roleSet = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
       List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
       for (Role role: roleSet) {
           grantedAuthorityList.add(new SimpleGrantedAuthority(role.getName()));
       }
       return grantedAuthorityList;
    }

    @Override
    public String getPassword() {
        return pass;
    }


    @Override
    public String getUsername() {
        return login;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
