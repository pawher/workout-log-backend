package com.pawher.workoutlog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProgressLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    private int weight;
    private LocalDate date;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private AppUser appUser;

    public ProgressLog(int weight, LocalDate date, AppUser appUser) {
        this.weight = weight;
        this.date = date;
        this.appUser = appUser;
    }
}
