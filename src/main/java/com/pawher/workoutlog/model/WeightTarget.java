package com.pawher.workoutlog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WeightTarget {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private int weightTarget;
    private LocalDate dateTarget;

    @OneToOne (fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private AppUser appUser;
}
