package com.pawher.workoutlog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Workout {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String description;
    private LocalDate workoutDate;
    private int time;

    public Workout(String description, LocalDate workoutDate, int time, WorkoutType workoutTypeSet, AppUser appUser) {
        this.description = description;
        this.workoutDate = workoutDate;
        this.time = time;
        this.workoutTypeSet = workoutTypeSet;
        this.appUser = appUser;
    }

    @OneToOne(fetch = FetchType.EAGER)
    private WorkoutType workoutTypeSet;

    @ManyToOne (fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private AppUser appUser;

}
