package com.pawher.workoutlog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "workou_type")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkoutType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    private String type;

    @ManyToOne (fetch = FetchType.EAGER)
    private AppUser appUser;

    public WorkoutType(String type) {
        this.type = type;
    }

    public WorkoutType(String type, AppUser appUser) {
        this.type = type;
        this.appUser = appUser;
    }
}
