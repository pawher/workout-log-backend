package com.pawher.workoutlog.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageResponse extends Response {
    private String message;
}
