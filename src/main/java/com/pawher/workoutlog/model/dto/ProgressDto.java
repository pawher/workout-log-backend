package com.pawher.workoutlog.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProgressDto {
    private long userId;
    private int weight;
    private LocalDate date;

}
