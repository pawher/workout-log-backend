package com.pawher.workoutlog.model.dto;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.ProgressLog;
import com.pawher.workoutlog.model.WeightTarget;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public abstract class RespFactory {
    public static ResponseEntity<Response> ok(String message) {
        return ResponseEntity.ok(new MessageResponse(message));

    }

    public static ResponseEntity<Response> created() {
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    public static ResponseEntity badRequest() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }

    public static <T> ResponseEntity<Response> result(PageResponse<T> response) {
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public static <T> ResponseEntity<T> result(T object) {
        return ResponseEntity.ok(object);
    }

    public static ResponseEntity<WeightTarget> badRequestWeight() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }

    public static ResponseEntity<ProgressLog> badRequestProgres() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    public static ResponseEntity<List<ProgressLog>> badRequestProsses() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }

    public static ResponseEntity<Integer> resultint(int number) {

        return ResponseEntity.status(HttpStatus.OK).body(number);
    }

    public static ResponseEntity<Long> resultLong(long days) {
        return ResponseEntity.status(HttpStatus.OK).body(days);
    }
}



