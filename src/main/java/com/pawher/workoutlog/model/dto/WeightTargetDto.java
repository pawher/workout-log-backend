package com.pawher.workoutlog.model.dto;

import com.pawher.workoutlog.model.WorkoutType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeightTargetDto {
    private long userId;
    private int weightTarget;
    private LocalDate dateTarget;

}
