package com.pawher.workoutlog.model.dto;

import com.pawher.workoutlog.model.WorkoutType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkoutDto {
    private long userId;
    private String description;
    private LocalDate workoutDate;
    private int time;
    private WorkoutType workoutType;


}
