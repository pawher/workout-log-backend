package com.pawher.workoutlog.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkoutTypeDto {

    private Long userId;
    private String workoutType;


}
