package com.pawher.workoutlog.model.response;

public enum StatusResponse {
    OK, SERVER_ERROR, REQUEST_ERROR
}
