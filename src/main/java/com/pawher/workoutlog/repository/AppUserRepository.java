package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Workout;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {


    Optional<AppUser> findByLogin (String username);
    Optional<AppUser> findByEmail (String email);

    Page<AppUser> findAllBy(Pageable pageable);
    Optional<AppUser> findById (Long id);

    Optional<AppUser> getAppUserById(Long id);

    Optional<AppUser> findByLoginAndPass(String username, String password);
}
