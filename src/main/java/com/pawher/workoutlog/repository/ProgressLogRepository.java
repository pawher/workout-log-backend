package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.ProgressLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProgressLogRepository extends JpaRepository<ProgressLog, Long> {
//    Optional<ProgressLog> findAllByAppUser(Long id);

    @Query(value = "SELECT progress_log.weight FROM progress_log where id=(select max(id) from progress_log);", nativeQuery = true)
    int getCurrentWeight ();


    List<ProgressLog> findAllByAppUser(AppUser user);

//    Integer findCurrentbyUser(AppUser user);
}
