package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
