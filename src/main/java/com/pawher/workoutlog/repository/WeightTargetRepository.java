package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.WeightTarget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface WeightTargetRepository extends JpaRepository<WeightTarget, Long> {
    Page<WeightTarget> findAllBy(Pageable pageable);

    @Query(value = "SELECT weight_target.weight_target FROM weight_target where id=(select max(id) from weight_target);", nativeQuery = true)
    int getMax ();


    @Query(value = "SELECT weight_target.date_target FROM weight_target where id=(select max(id) from weight_target);", nativeQuery = true)
    LocalDate getDate();

    List<WeightTarget> findAllByAppUser(AppUser user);
}
