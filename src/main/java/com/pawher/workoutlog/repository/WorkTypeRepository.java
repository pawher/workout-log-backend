package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.WorkoutType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WorkTypeRepository extends JpaRepository<WorkoutType, Long> {
    Page<WorkoutType> findAllBy(Pageable pageable);
    Optional<WorkoutType> findByType(String type);
}
