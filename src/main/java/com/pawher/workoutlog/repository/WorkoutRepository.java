package com.pawher.workoutlog.repository;

import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface WorkoutRepository extends JpaRepository<Workout, Long> {
    @Override
    Optional<Workout> findById(Long aLong);

    PageResponse<Workout> findAllBy();

    @Query(value = "SELECT SUM(workout.time) FROM workout;", nativeQuery = true)
    Integer getTime();


    @Query(value = "SELECT count(workout_type_set_id) FROM workout where workout_type_set_id=3", nativeQuery = true)
    Integer getGymCounter();

    Integer countAllByWorkoutTypeSet(WorkoutType type);

    Page<Workout> getAllByOrderByWorkoutDateDesc(Pageable pageable);
}

