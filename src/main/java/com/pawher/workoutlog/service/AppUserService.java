package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.RegistrationException;
import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.exceptions.UserEmailAlereadyExistsExeption;
import com.pawher.workoutlog.exceptions.UserLoginAlereadyExistsExeption;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.dto.LoginDto;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;



@Service
public class AppUserService implements IAppUserService {

    private static final int DEFAULT_PAGE_SIZE = 5;
    private AppUserRepository appUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public AppUserService(AppUserRepository appUserRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.appUserRepository = appUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void register(AppUser appUser) throws RegistrationException {
        appUser.setEmail(appUser.getEmail().toLowerCase());
        appUser.setLogin(appUser.getLogin().toLowerCase());
        Optional<AppUser> emailUser = appUserRepository.findByEmail(appUser.getEmail());
        if (emailUser.isPresent()) {
            throw new UserEmailAlereadyExistsExeption();
        }
        Optional<AppUser> loginUser = appUserRepository.findByLogin(appUser.getLogin());
        if (loginUser.isPresent()) {
            throw new UserLoginAlereadyExistsExeption();
        }

        appUser.setPass(bCryptPasswordEncoder.encode(appUser.getPass()));

        appUserRepository.save(appUser);

    }

    public Optional<AppUser> getUserWithId (Long id) {
        return appUserRepository.findById(id);
    }

    @Override
    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) throws UserDoesNotExistException{
        Optional<AppUser> foundUser = appUserRepository.findByLogin(dto.getUsername());

        if (!foundUser.isPresent()) {
            throw new UserDoesNotExistException();
        } else {
            AppUser user = foundUser.get(); // wydobywam uzytkownika
            // sprawdzam (nizej) czy haslo zgadza sie z tym z bazy danych
            if (!bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
                // jesli nie zgadza sie haslo to exception
                throw new UserDoesNotExistException();

                // jesli sie zgadza to pomijam i zakonczy metodę
            }
        }

        return foundUser;
    }

    @Override
    public PageResponse<AppUser> getAllUsers() {
        return getUsers(0);
    }

    @Override
    public PageResponse<AppUser> getUsers (int page) {
        Page<AppUser> users = appUserRepository.findAllBy(PageRequest.of(page, DEFAULT_PAGE_SIZE));
        return new PageResponse<>(users);
    }

}
