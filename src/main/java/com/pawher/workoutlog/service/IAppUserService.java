package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.RegistrationException;
import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.dto.LoginDto;
import com.pawher.workoutlog.model.dto.PageResponse;

import java.util.Optional;

public interface IAppUserService  {
     void register(AppUser appUser) throws RegistrationException;
    PageResponse<AppUser> getAllUsers();
    PageResponse<AppUser> getUsers (int page);
    Optional<AppUser> getUserWithId (Long id);

    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) throws UserDoesNotExistException;
}
