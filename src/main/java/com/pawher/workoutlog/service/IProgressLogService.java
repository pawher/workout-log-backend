package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.ProgressLog;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.ProgressDto;

import java.util.List;

public interface IProgressLogService {


    PageResponse<ProgressLog> getAllProgresses();
    PageResponse<ProgressLog> getProgress(int page);
     void addNewProgress(ProgressDto dto) throws UserDoesNotExistException;

    List<ProgressLog> getAllProgressesByUserId(Long id) throws UserDoesNotExistException;

    Integer getCurrent();

    Integer getCurrentByUserID(Long id) throws UserDoesNotExistException;

//    Integer getCurrentByUserId(Long userId) throws UserDoesNotExistException;
}
