package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.exceptions.WeightTargetDoesNotExistsException;
import com.pawher.workoutlog.model.WeightTarget;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WeightTargetDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IWeightTargetService {
    void addWeightTarget (WeightTargetDto weightTarget) throws UserDoesNotExistException;
    Optional<WeightTarget> getWeightTargetByUserID(Long id);
    void updateWeightTarget (WeightTarget weightTarget, Long id) throws WeightTargetDoesNotExistsException, UserDoesNotExistException;

    PageResponse<WeightTarget> getAllTargets();
    PageResponse<WeightTarget> getTargets(int page);


    Integer getAllTargetsAsList();

    LocalDate getTargetDate();

    Integer getAllTargetsAsListByUserID(Long id) throws UserDoesNotExistException;
}
