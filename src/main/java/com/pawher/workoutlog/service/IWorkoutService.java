package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WorkoutDto;

import java.util.Optional;

public interface IWorkoutService {

    PageResponse<Workout> getAllWorkouts();
    PageResponse<Workout> getWorkout (int page);
    Optional<Workout> getWorkoutByID(Long id);

    void remove(Workout workout);

    void addNewWorkout(WorkoutDto workout) throws UserDoesNotExistException;


    Integer getTime();

    Integer getWorkoutType();

    Optional<WorkoutType> getWorkoutByName(String workoutType);

    int getWorkoutsCountByType(WorkoutType workoutType);
}

