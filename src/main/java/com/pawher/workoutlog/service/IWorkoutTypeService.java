package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WorkoutTypeDto;

public interface IWorkoutTypeService {
    PageResponse<WorkoutType> getAllType();

    PageResponse<WorkoutType> getWorkType(int page);

    void addNewType(WorkoutTypeDto workoutTypeDto) throws UserDoesNotExistException;
}
