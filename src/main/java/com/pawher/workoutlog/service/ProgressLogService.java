package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.ProgressLog;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.ProgressDto;
import com.pawher.workoutlog.repository.ProgressLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProgressLogService implements IProgressLogService {

    private static final int DEFAULT_PAGE_SIZE = 100;
    private ProgressLogRepository progressLogRepository;
    private AppUserService appUserService;

    @Autowired
    public ProgressLogService(ProgressLogRepository progressLogRepository, AppUserService appUserService) {
        this.progressLogRepository = progressLogRepository;
        this.appUserService = appUserService;
    }

    @Override
    public void addNewProgress(ProgressDto dto) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(dto.getUserId());
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser user = appUser.get();
        ProgressLog progressLog = new ProgressLog(dto.getWeight(), dto.getDate(), user);
        progressLogRepository.save(progressLog);
    }

    @Override
    public List<ProgressLog> getAllProgressesByUserId(Long id) throws UserDoesNotExistException {
        Optional<AppUser> appUserOptional = appUserService.getUserWithId(id);

        if (appUserOptional.isPresent()) {
            AppUser user = appUserOptional.get();

            return user.getProgressLogList().stream().collect(Collectors.toList());
        }
        throw new UserDoesNotExistException();
    }

    @Override
    public Integer getCurrent() {
        return progressLogRepository.getCurrentWeight();
    }

    @Override
    public Integer getCurrentByUserID(Long id) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(id);
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser user = appUser.get();

        List<ProgressLog> progressLogList = progressLogRepository.findAllByAppUser(user);
        Optional<ProgressLog> progressLog = progressLogList.stream()
                .sorted((v1, v2) -> {
                    if (v1.getDate().isAfter(v2.getDate())) {
                        return -1;
                    } else if (v2.getDate().isAfter(v1.getDate())) {
                        return 1;
                    }
                    return 0;
                }).findFirst();
        if (progressLog.isPresent()) {
            return progressLog.get().getWeight();
        } else {
            throw new UserDoesNotExistException();
        }
    }

//    @Override
//    public Integer getCurrentByUserId(Long userId) throws UserDoesNotExistException {
//        Optional<AppUser> appUser = appUserService.getUserWithId(userId);
//        if (!appUser.isPresent()) {
//            throw new UserDoesNotExistException();
//        }
//        AppUser user = appUser.get();
//
//
//
//        return progressLogRepository.findCurrentbyUser(user);
//    }


    @Override
    public PageResponse<ProgressLog> getAllProgresses() {
        return getProgress(0);
    }

    @Override
    public PageResponse<ProgressLog> getProgress(int page) {
        Page<ProgressLog> progressLogs = progressLogRepository.findAll(PageRequest.of(page, DEFAULT_PAGE_SIZE));
        return new PageResponse<>(progressLogs);
    }


}
