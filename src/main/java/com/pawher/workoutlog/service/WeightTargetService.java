package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.exceptions.WeightTargetDoesNotExistsException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.ProgressLog;
import com.pawher.workoutlog.model.WeightTarget;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WeightTargetDto;
import com.pawher.workoutlog.repository.WeightTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class WeightTargetService implements IWeightTargetService {

    private static final int DEFAULT_PAGE_SIZE = 100;

    private WeightTargetRepository weightTargetRepository;
    private IAppUserService appUserService;

    @Autowired
    public WeightTargetService(WeightTargetRepository weightTargetRepository, IAppUserService appUserService) {
        this.weightTargetRepository = weightTargetRepository;
        this.appUserService = appUserService;
    }

    @Override
    public void addWeightTarget(WeightTargetDto weightTarget) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(weightTarget.getUserId());
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser user = appUser.get();
        WeightTarget weightTarget1 = new WeightTarget(weightTarget.getUserId(),weightTarget.getWeightTarget(),weightTarget.getDateTarget(),user);
        weightTargetRepository.save(weightTarget1);
    }

    @Override
    public Optional<WeightTarget> getWeightTargetByUserID(Long id) {
        Optional<AppUser> appUserOptional = appUserService.getUserWithId(id);
        if (appUserOptional.isPresent()) {
            AppUser user = appUserOptional.get();
            return Optional.ofNullable(user.getWeightTarget());
        }

        return Optional.empty();
    }

    @Override
    public void updateWeightTarget(WeightTarget weightTarget, Long id) throws WeightTargetDoesNotExistsException, UserDoesNotExistException {
        Optional<AppUser> userById = appUserService.getUserWithId(id);
        Optional<WeightTarget> targetById = weightTargetRepository.findById(weightTarget.getId());

        if (userById.isPresent()) {
            if (targetById.isPresent()) {
                weightTargetRepository.save(weightTarget);
            } else {
                throw new WeightTargetDoesNotExistsException();
            }
        } else {
            throw new UserDoesNotExistException();
        }
    }

    @Override
    public PageResponse<WeightTarget> getAllTargets() {
        return getTargets(0);
    }

@Override
    public PageResponse<WeightTarget> getTargets(int page) {
        Page<WeightTarget> targets = weightTargetRepository.findAllBy(PageRequest.of(page,DEFAULT_PAGE_SIZE ));
        return new PageResponse<>(targets);
    }

    @Override
    public Integer getAllTargetsAsList() {
        Integer getmax = weightTargetRepository.getMax();
        return getmax;
    }

    @Override
    public LocalDate getTargetDate() {
        LocalDate targetDate = weightTargetRepository.getDate();
        return targetDate;
    }

    @Override
    public Integer getAllTargetsAsListByUserID(Long id) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(id);
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser user = appUser.get();

        List<WeightTarget> weightTargetList = weightTargetRepository.findAllByAppUser(user);
        Optional<WeightTarget> weightTarget = weightTargetList.stream()
                .sorted((v1, v2) -> {
                    if (v1.getDateTarget().isAfter(v2.getDateTarget())) {
                        return -1;
                    } else if (v2.getDateTarget().isAfter(v1.getDateTarget())) {
                        return 1;
                    }
                    return 0;
                }).findFirst();
        if (weightTarget.isPresent()) {
            return weightTarget.get().getWeightTarget();
        } else {
            throw new UserDoesNotExistException();
        }
    }
}
