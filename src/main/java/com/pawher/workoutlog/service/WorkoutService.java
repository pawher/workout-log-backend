package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WorkoutDto;
import com.pawher.workoutlog.repository.AppUserRepository;
import com.pawher.workoutlog.repository.WorkTypeRepository;
import com.pawher.workoutlog.repository.WorkoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WorkoutService implements IWorkoutService {

    private static final int DEFAULT_PAGE_SIZE = 5;


    @Autowired
    private WorkoutRepository workoutRepository;
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private WorkTypeRepository workTypeRepository;


    @Override
    public PageResponse<Workout> getAllWorkouts() {
        return getWorkout(0);
    }

    @Override
    public PageResponse<Workout> getWorkout(int page) {
        Page<Workout> workouts = workoutRepository.getAllByOrderByWorkoutDateDesc(PageRequest.of(page, DEFAULT_PAGE_SIZE));
        return new PageResponse<>(workouts);
    }

    @Override
    public Optional<Workout> getWorkoutByID(Long id) {
        return workoutRepository.findById(id);

    }

    @Override
    public void remove(Workout workout) {
        workoutRepository.delete(workout);

    }

    @Override
    public void addNewWorkout(WorkoutDto dto) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(dto.getUserId());
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }

        WorkoutType typeFromDB = null;
        Optional<WorkoutType> type = workTypeRepository.findByType(dto.getWorkoutType().getType());
        if(type.isPresent()){
            typeFromDB = type.get();
        }
        AppUser user = appUser.get();
        Workout workout1 = new Workout(dto.getDescription(), dto.getWorkoutDate(), dto.getTime(), typeFromDB, user);
        workoutRepository.save(workout1);
        user.getWorkoutList().add(workout1);
        appUserRepository.save(user);

    }

    @Override
    public Integer getTime() {
        Integer getTime = workoutRepository.getTime();
        return getTime;
    }

    @Override
    public Integer getWorkoutType() {
        Integer getGymCounter = workoutRepository.getGymCounter();
        return getGymCounter;
    }

    @Override
    public Optional<WorkoutType> getWorkoutByName(String workoutType) {
        return workTypeRepository.findByType(workoutType);
    }

    @Override
    public int getWorkoutsCountByType(WorkoutType workoutType) {
        return workoutRepository.countAllByWorkoutTypeSet(workoutType);
    }


}
