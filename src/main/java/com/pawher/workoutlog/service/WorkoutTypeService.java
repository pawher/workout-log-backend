package com.pawher.workoutlog.service;

import com.pawher.workoutlog.exceptions.UserDoesNotExistException;
import com.pawher.workoutlog.model.AppUser;
import com.pawher.workoutlog.model.Workout;
import com.pawher.workoutlog.model.WorkoutType;
import com.pawher.workoutlog.model.dto.PageResponse;
import com.pawher.workoutlog.model.dto.WorkoutTypeDto;
import com.pawher.workoutlog.repository.WorkTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WorkoutTypeService implements IWorkoutTypeService {

    private static final int DEFAULT_PAGE_SIZE = 100;

    private WorkTypeRepository workTypeRepository;
    private AppUserService appUserService;

    @Autowired
    public WorkoutTypeService(WorkTypeRepository workTypeRepository, AppUserService appUserService) {
        this.workTypeRepository = workTypeRepository;
        this.appUserService = appUserService;
    }



    @Override
    public PageResponse<WorkoutType> getAllType() {
        return getWorkType(0);
    }

    @Override
    public PageResponse<WorkoutType> getWorkType(int page) {
        Page<WorkoutType> workoutTypes = workTypeRepository.findAllBy(PageRequest.of(page, DEFAULT_PAGE_SIZE));
        return new PageResponse<>(workoutTypes);
    }

    @Override
    public void addNewType(WorkoutTypeDto workoutTypeDto) throws UserDoesNotExistException {
        Optional<AppUser> appUser = appUserService.getUserWithId(workoutTypeDto.getUserId());
        if (!appUser.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser user = appUser.get();
        WorkoutType workoutType = new WorkoutType(workoutTypeDto.getWorkoutType(), user);
        workTypeRepository.save(workoutType);
    }
}
